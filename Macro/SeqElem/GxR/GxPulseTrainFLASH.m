
function [GAmp,GTime]=GxPulseTrainFLASH(p)

global VCtl;
global VObj;
global VVar;

train_length = 6;
TimeOffset = p.tMiddle-train_length/2*p.tSpacing;
GAmp=[];
GTime=[];
for i = 1:train_length
    tStart = TimeOffset + (i-1)*p.tSpacing;
    tEnd = tStart + p.tSpacing;
    [GAmpt,GTimet]=StdTrap(tStart, tEnd, tStart+p.tRamp, tEnd-p.tRamp, ...
                           p.GxAmp,max(2, p.sRamp),2,max(2, p.sRamp));
    GAmp=[GAmp GAmpt*(-1)^(i-1)];
    GTime=[GTime GTimet];
end

[GTime,m,n]=unique(GTime);
GAmp=GAmp(m);

end
