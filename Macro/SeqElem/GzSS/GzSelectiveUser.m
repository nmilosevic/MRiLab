
function [GAmp,GTime] = GzSelectiveUser(p)

global VCtl;

tFastRamp1=p.tFastRamp;
tFastRamp2=p.tFastRamp;
tFastRamp3=p.tFastRamp;
tFastRamp4=p.tFastRamp;
tStart=p.tStart;

tLeftCrusher=p.tLeftCrush;
tLeftRamp=p.tLeftRamp;
tTrapFlat=p.tTrapFlat;
tRightRamp=p.tRightRamp;
tRightCrusher=p.tRightCrush;

ampLeftCrusher=p.ampLeftCrush;
ampTrapLeft=p.ampTrapLeft;
ampTrapFlat=p.ampTrapFlat;
ampTrapRight=p.ampTrapRight;
ampRightCrusher=p.ampRightCrush;

pointsPerLine = 2;

Duplicates=max(1,p.Duplicates);
DupSpacing=max(0,p.DupSpacing);

if tLeftCrusher == 0
    ampLeftCrusher = ampTrapLeft;
    tFastRamp2 = 0;
end

if tRightCrusher == 0
    ampRightCrusher = ampTrapLeft;
    tFastRamp3 = 0;
end

% left crusher
p1 = tStart;
p2 = p1 + tFastRamp1;
[GAmp1, GTime1] = ramp(p1, p2, 0, ampLeftCrusher, pointsPerLine);
p1 = p2;
p2 = p2 + tLeftCrusher;
[GAmp2, GTime2] = rect(p1, p2, ampLeftCrusher, pointsPerLine);
p1 = p2;
p2 = p2 + tFastRamp2;
[GAmp3, GTime3] = ramp(p1,   ...
                       p2, ...
                       ampLeftCrusher, ampTrapLeft, pointsPerLine);

% trapezoid selective gradient
p1 = p2;
p2 = p2 + tLeftRamp;
[GAmp4, GTime4] = ramp(p1, ...
                       p2, ...
                       ampTrapLeft, ampTrapFlat, pointsPerLine);
p1 = p2;
p2 = p2 + tTrapFlat;
[GAmp5, GTime5] = rect(p1, ...
                       p2, ...
                       ampTrapFlat, pointsPerLine);
p1 = p2;
p2 = p2 + tRightRamp;
[GAmp6, GTime6] = ramp(p1, ...
                       p2, ...
                       ampTrapFlat, ampTrapRight, pointsPerLine);

% right crusher
p1 = p2;
p2 = p2 + tFastRamp3;
[GAmp7, GTime7] = ramp(p1, ...
                       p2, ...
                       ampTrapRight, ampRightCrusher, pointsPerLine);
p1 = p2;
p2 = p2 + tRightCrusher;
[GAmp8, GTime8] = rect(p1, ...
                       p2, ...
                       ampRightCrusher, pointsPerLine);
p1 = p2;
p2 = p2 + tFastRamp4;
[GAmp9, GTime9] = ramp(p1, ...
                       p2, ...
                       ampRightCrusher, 0, pointsPerLine);

GAmp=[GAmp1 GAmp2 GAmp3 GAmp4 GAmp5 GAmp6 GAmp7 GAmp8 GAmp9];
t=[GTime1 GTime2 GTime3 GTime4 GTime5 GTime6 GTime7 GTime8 GTime9];

[GTime,m,~]=unique(t);
GAmp=GAmp(m);

% Create Duplicates
if Duplicates~=1 & DupSpacing ~=0
    GAmp=repmat(GAmp,[1 Duplicates]);
    TimeOffset = repmat(0:DupSpacing:(Duplicates-1)*DupSpacing,[length(GTime) 1]);
    GTime=repmat(GTime,[1 Duplicates]) + (TimeOffset(:))';
end

end

function [Grad,t]=ramp(t1,t2,amp1,amp2,step)

t=linspace(t1,t2,step);
Grad=linspace(amp1,amp2,step); %ramp


end

function [Grad,t]=rect(t1,t2,amp,step)

t=linspace(t1,t2,step);
Grad=amp*ones(size(t)); % rectangle

end
